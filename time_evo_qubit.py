import numpy as np
from numpy.linalg import matrix_power
import scipy.sparse
import itertools
import qiskit
from tqdm.auto import tqdm
import math
from numpy import linalg as LA
import pandas as pd
import multiprocessing
import cirq
from cirq.circuits import InsertStrategy
from scipy.sparse import csr_matrix
from scipy import sparse

#defining Pauli matrices
sx = np.array([[0,1],[1,0]])
sz = np.array([[1,0],[0,-1]])

#defining number of particles
N=5
qubits = [cirq.LineQubit(i) for i in range(2*N)]

circuit = cirq.Circuit()
X = cirq.X
Z = cirq.Z
Y = cirq.Y
I = cirq.I

#circuit = cirq.Circuit()

sigma = [I,X,Y,Z]
n = [0, 1, 2, 3]
ind = np.array([p for p in itertools.product(n, repeat=2*N)])
print(ind)
S_collect = []
for i in tqdm(range (0, len(ind))):
    S = cirq.PauliString(sigma[ind[i][0]](qubits[0]))

    for j in range(1,2*N):
        S = S *cirq.PauliString(sigma[ind[i][j]](qubits[j]))  

    
    
        
    S_collect.append(S)

#defining parameters of the H
J = 1.            
hx = 0.5         



#defining Tensor product to use in Hamiltonian
def tensor_prod(idx, s, size=N):
    Id = np.array([[1,0],[0,1]])
    idx, s = np.array(idx), np.array(s)
    matrices = [Id if k not in idx else s for k in range(size)]
    product = matrices[0]
    for k in range(1, size):
        product = np.kron(product, matrices[k])
    return product

#defining Hamiltonian with number of particles N
def Hamiltonian(N):
    H = (-J*sum([tensor_prod([k, k+1], sz, size=N) for k in range(N-1)]) - 
    hx*sum([tensor_prod(k, sx, size=N) for k in range(N)]) )
    return H

#saving the Hamiltonian
H = Hamiltonian(N)


i0 = np.array([np.zeros(2**(N),dtype=complex)]).T
psi0 = np.kron(i0,i0)
for i in range(2**N):
    i00 = np.array([np.zeros(2**(N),dtype=complex)]).T
    i00[i][0] = 1+0j
    psi0 += np.kron(i00,i00)  

psi1 = psi0/np.linalg.norm(psi0)

# #function of calculating OTOC for each time t
def function(t): 
    XX = np.array([[0.,1.],
                    [1.,0.]], dtype=complex)

    ZZ = np.array([[1.,0.],
                    [0.,-1.]], dtype=complex)

    YY = np.array([[0,-1j],
                    [1j,0]], dtype=complex)
    
    
    Hadamard = cirq.H
    Phase = cirq.S
    T_gate = cirq.T
    X = cirq.X
    Z = cirq.Z
    Y = cirq.Y
    I_gate = cirq.I
    CNOT = cirq.CNOT
    single_gates = [Hadamard,Phase,X,Z,Y,I_gate]
    
    NUMBER = []
    otoc = []
    nt =[]
    qubits = [cirq.LineQubit(i) for i in range(N)]

    
    rep =0
    while rep<500:
        
        circuit = cirq.Circuit()  
    
    
        #producing the random Clifford circuit with certain depth   
    
        depthC = 30
        for dd in range(depthC):
            q = np.random.randint(0,len(single_gates),N)
            for i in range(N):
                circuit.append(single_gates[q[i]].on(qubits[i]), strategy=InsertStrategy.NEW)


            control = []            
            while len(control)<2:
                qq = np.random.randint(0,N,2)
                if qq[0] != qq[1]:
                    control.append(qq)
                    circuit.append(CNOT.on(qubits[qq[0]],qubits[qq[1]]), strategy=InsertStrategy.NEW)
        
            
        circuit2 = cirq.Circuit()  
    
    
        #producing the random circuit with certain depth   
    
        depthC = 30
        for dd in range(depthC):
            q = np.random.randint(0,len(single_gates),N)
            for i in range(N):
                circuit2.append(single_gates[q[i]].on(qubits[i]), strategy=InsertStrategy.NEW)


            control2 = []            
            while len(control)<2:
                qq = np.random.randint(0,N,2)
                if qq[0] != qq[1]:
                    control2.append(qq)
                    circuit2.append(CNOT.on(qubits[qq[0]],qubits[qq[1]]), strategy=InsertStrategy.NEW)                        

    



        
        s=cirq.Simulator()
        results=s.simulate(circuit)
        results2=s.simulate(circuit2)
        
        #final state of the circuit
        psi_stab = results.final_state_vector
        
        #time evolution operator U_H (t) of the Hamiltonian
        matrix_H = csr_matrix(qiskit.extensions.HamiltonianGate(H,t).to_matrix())
        matrix_C = csr_matrix(cirq.unitary(circuit))
        matrix_C2 = csr_matrix(cirq.unitary(circuit2))
        U = matrix_C @ matrix_H @ matrix_C2

        #final state of U_C2 U_H .U_C
        psi= matrix_C2 @  matrix_H @  csr_matrix(np.array([psi_stab]).T) 
        
        butterfly = csr_matrix(np.ones((1,1)))
        for i in range (N-2):
            butterfly = sparse.kron(csr_matrix(np.identity(2)),butterfly)
        butterfly = sparse.kron(butterfly,XX)
        butterfly = sparse.kron(butterfly,csr_matrix(np.identity(2)))
        
        #calculting O=U^\dagger. B.U
        O = U.conjugate().T @ butterfly @ U
        

        #producing M operator
        M = ZZ 
        for i in range(N-1):
            M = np.kron(M, np.identity(2))
        M = csr_matrix(M)
        #Calculating OTOC=<O^\dagger .M^\dagger . O.M>
        OTOC = psi.conjugate().T @ O.conjugate().T @ M.conjugate().T @ O @ M @ psi
        
        #saving real part of OTOC
        otoc.append(OTOC.toarray()[0][0].real)
        #stabilizer Renyi entropy calculation

        identity = np.identity(2**(N),dtype=complex)

        M2 = []
        if rep<3:
            ss=0

        
            # print(psi0.shape)
            print(psi.toarray().shape)
            sta = np.kron(identity,  U.toarray()) @ psi1
            qubits = [cirq.LineQubit(i) for i in range(2*N)]
            for i in range(len(S_collect)):
                st = cirq.PauliSum.from_pauli_strings([S_collect[i]])
                expectation = st.expectation_from_state_vector(sta.T [0] ,qubit_map={qubits[0]:0,qubits[1]:1,qubits[2]:2,qubits[3]:3,qubits[4]:4,qubits[5]:5,qubits[6]:6,qubits[7]:7,qubits[8]:8,qubits[9]:9 })
                ss += 2**(-2*N) * np.array(expectation).real **4
                
            m2 = -np.log2(ss)
             
            M2.append(m2)

            pd.DataFrame(np.asarray(M2)).to_csv(f'qubit_data_final_time_evo/Magic_sc_N{N}_qubit_time_evo_{int(t*20)}_rep_{rep}_double_cliff_Chio_500_test_run.csv')
        print("rep = ", rep)
        rep += 1
        
        
        
       

        
    #Saving the data   
    pd.DataFrame(np.asarray(otoc)).to_csv(f'qubit_data_final_time_evo/OTOC_sc_N{N}_qubit_time_evo_{int(t*20)}_double_cliff_Choi_500.csv')
   



     
if __name__ == '__main__':

    pool = multiprocessing.Pool()
    
    t= [t/20 for t in range(60)]
    outputs_async = pool.map_async(function, t)
    outputs = outputs_async.get()
    print("Output: {}".format(outputs))
