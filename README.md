# MagicInfoScrambling

Code for the manuscript "Quantifying non-stabilizerness efficiently via information scrambling".

############################### Random Qutrit Circuit Code and Data ##################################################

The python script 'qutrit_OTOC.py' contains the codes required to simulate a quantum circuit for 3 dimensional Hilbert space, with the Clifford gates and non-Clifford gates of  T-gate, for calculating Mana and OTOC. Depending on the availble memory of your device, you can use this code to simulate a system with handful qutrits. The generated data from this file are avalible on the folders "OTOC_Mana_6"

These data have been used on Fig.2 of the manuscript.




############################### Random Qubit Circuit Code and Data ###################################################

The python script 'qubit_OTOC_magic_op.py' contains the codes required to simulated a quantum circuit for 2 dimnsional Hilbert space, with the Clifford gates and on-Clifford gates of  T-gate, for calculating OTOC and Stabilizer Renyi Entropy.  Depending on the availble memory of your device, you can use this code to simulate a system with handful qubits. The genrated data from this code are avalible on the folder "qubit_data_final" and "qubit_data_final_14". 

These data have been used on Fig.3  of the manuscript.


The python script 'time_evo_qubit.py' contains the codes required to simulated a quantum circuit for 2 dimnsional Hilbert space, with the Clifford gates and time evolution of the Hamiltonian, for calculating OTOC and Stabilizer Renyi Entropy.  Depending on the availble memory of your device, you can use this code to simulate a system with handful qubits. The genrated data from this code are avalible on the folder "qubit_data_final_time_evo". 

These data have been used on Fig.4 of the manuscript.

############################## Plot codes #############################################################

the jupyter notebook 'plot_notebok.ipynb' containes the code required to plot the figures of the manuscript and its figures are avalible on the folder "plots".
