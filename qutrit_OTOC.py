import numpy as np
from numpy.linalg import matrix_power
import scipy.sparse
import itertools
from tqdm.auto import tqdm
from numpy import linalg as LA
import pandas as pd
import multiprocessing
import cirq
from cirq.circuits import InsertStrategy
from scipy.sparse import csr_matrix


#Classes of Required gates for Cirq in 3-D Hilbert space

#Pauli X gate
class X_gate(cirq.SingleQubitGate):
    def _qid_shape_(self):
        return (3,)

    def _unitary_(self):
        return np.array([[0,0,1],
                         [1,0,0],
                         [0,1,0]],dtype=complex)

    def _circuit_diagram_info_(self, args):
        return '[X]'
#Pauli Z gate
class Z_gate(cirq.SingleQubitGate):
    def _qid_shape_(self):
        return (3,)

    def _unitary_(self):
        return ZZ

    def _circuit_diagram_info_(self, args):
        return '[Z]'

#Hadamard gate
class Hadamrad(cirq.SingleQubitGate):
    def _qid_shape_(self):
        return (3,)

    def _unitary_(self):
        return 1/np.sqrt(3) *np.array([[1,1,1],
                                       [1,w,w**2],
                                       [1,w**2,w]],dtype=complex)

    def _circuit_diagram_info_(self, args):
        return '[H]'

#Phase gate
class K_gate(cirq.SingleQubitGate):
    def _qid_shape_(self):
        return (3,)

    def _unitary_(self):
        return np.array([[1,0,0],
                         [0,1,0],
                         [0,0,w]],dtype=complex)
    def _circuit_diagram_info_(self, args):
        return '[K]'


#Identity gate
class I_gate(cirq.SingleQubitGate):
    def _qid_shape_(self):
        return (3,)

    def _unitary_(self):
        return np.array([[1,0,0],
                         [0,1,0],
                         [0,0,1]],dtype=complex)
    def _circuit_diagram_info_(self, args):
        return '[I]'


#Entangling Sum gate
class Sum(cirq.TwoQubitGate):
    def _qid_shape_(self):
        return (3,3)
    def __init__(self):
        super(Sum, self)

    def _unitary_(self):
        gate = np.zeros((9,9),dtype=complex)
        for i in range(3):
            for j in range(3):
                gate += np.kron(base[i],base[(i+j)%3]) @ np.kron(base[i].T, base[j].T)
        
        return gate

    def _circuit_diagram_info_(self, args):
        return "[C]", "[SUM]"

#Non-Clifford Rotation-Z gate
class RotZ_gate(cirq.SingleQubitGate):
    def _qid_shape_(self):
        return (3,)

    def _unitary_(self):
        return np.array([[1,0,0],
                         [0,np.exp(1j*theta),0],
                         [0,0,1]],dtype=complex)

    def _circuit_diagram_info_(self, args):
        return '[$\\theta Z]'

#Non-Clifford T-gate
class T_gate(cirq.SingleQubitGate):
    def _qid_shape_(self):
        return (3,)

    def _unitary_(self):
        return np.array([[np.exp(-2*np.pi*1j/9),0,0],
                         [0,1,0],
                         [0,0,np.exp(2*np.pi*1j/9)]],dtype=complex)

    def _circuit_diagram_info_(self, args):
        return '[T]'

#list of single Clifford gates being used
single_gates = [X_gate(),Z_gate(),Hadamrad(),K_gate(),I_gate()]

#Number of Qutirts
N=6
w = np.exp(2*np.pi*1j/3)

X = np.array([[0,0,1],
             [1,0,0],
             [0,1,0]], dtype=complex)

x0 = np.array([[1],[0],[0]], dtype=complex)
x1 = np.array([[0],[1],[0]], dtype=complex)
x2 = np.array([[0],[0],[1]], dtype=complex)
base = [x0,x1,x2]

ZZ = np.zeros((3,3),dtype = complex)
XX = np.array([[0,0,1],
            [1,0,0],
            [0,1,0]], dtype=complex)

ZZ[0][0]=1
ZZ[1][1]= np.exp(2*np.pi*1j/3)
ZZ[2][2]=np.exp(2*np.pi*1j/3)*np.exp(2*np.pi*1j/3)

Z = csr_matrix(ZZ)
X = csr_matrix(XX)



w = np.exp(2*np.pi*1j/3)
T = np.zeros((3,3),dtype = complex)
        

        #defining generalized Pauli functions 
def t(i,j):
    TT=w**(-2*i*j)*np.dot(matrix_power(ZZ,i),matrix_power(XX,j))
    return TT


x=[0, 1, 2]

ind = np.array([p for p in itertools.product(x, repeat=2*N)])
        

ind_list = ind.tolist
T_collect = []
print("Producing Generalized Pauli Strings:")
        #definig Pauli strings
       
for i in tqdm(range (0, len(ind))):
    T=np.ones((1,1), dtype = complex)
    l=ind[i,:].tolist()

    res = np.array_split(l,N)
    for j in res:
        T = np.kron(T,t(j[0],j[1]))
    
        if T.shape[0] == 3**N:
        
            T_collect.append(csr_matrix(T))
       


        
        #summation of all Pauli strings
T_sum = sum(T_collect)

print ("Producing phase space point operators:")
#calculating phase space point operators 
A=[]
for Tu in tqdm(T_collect):
    Au = 3**(-N)* (Tu @ T_sum) @ csr_matrix.conjugate(csr_matrix.transpose(Tu)) 
    
    A.append(csr_matrix(np.round(Au.todense(),6)))

T_collect = [0]






    

#function of calculating OTOC and Mana for probability = rnd
def function (depth):
        
    NUMBER = []
    otoc = []
    ima = []
    reall = []
    
    

    rep =0
    while rep<sample:
        qudits = [cirq.LineQid(i,dimension=3) for i in range(N)]
        circuit = cirq.Circuit()    
        
        #producing the random circuit with certain depth 
        for d in range(depth):
            depthC = 10
            for dd in range(depthC):
                q = np.random.randint(0,len(single_gates),N)
                for i in range(N):
                    circuit.append(single_gates[q[i]].on(qudits[i]), strategy=InsertStrategy.NEW)


                control = []
                if np.random.rand()<1:
                    while len(control)<2:
                        qq = np.random.randint(0,N,2)
                        if qq[0] != qq[1]:
                            control.append(qq)
                            circuit.append(Sum().on(qudits[qq[0]],qudits[qq[1]]), strategy=InsertStrategy.NEW)
                
            
                        
            if np.random.rand()<1:
                t_rand = np.random.randint(0,N,1)
                circuit.append(RotZ_gate().on(qudits[t_rand[0]]), strategy=InsertStrategy.NEW)

        #unitary matrix of the circuit, U
        matrix = cirq.unitary(circuit)

        s=cirq.Simulator()
        results=s.simulate(circuit)
        
        #final state of the circuit
        psi = results.final_state_vector
        
        re_psi = np.array([psi]).real
        im_psi = np.array([psi]).imag


        #producing butterfly operator
        butterfly = np.ones((1,1))
        for i in range (N-2):
            butterfly = np.kron(np.identity(3),butterfly)
        butterfly = np.kron(butterfly,XX)
        butterfly = np.kron(butterfly,np.identity(3))
        
        #calculting O=U^\dagger. B.U 
        O = np.conjugate(matrix).T @ butterfly @ matrix
        
        #producing M operator
        M = ZZ 
        for i in range(N-1):
            M = np.kron(M, np.identity(3))
        
        #Calculating OTOC=<O^\dagger .M^\dagger . O.M>
        OTOC = np.conjugate(np.array([psi])) @ np.conjugate(O).T @ np.conjugate(M).T @ O @ M @ np.array([psi]).T
        
        #saving real part of OTOC
        otoc.append(OTOC[0][0].real)
        Mana = []
<<<<<<< HEAD
        if rep<3:
=======
        if rep<10:
>>>>>>> 1e98111a7cb936b77f4ecec908b7031c65b5d90d
            m=0
            rho = np.array([psi]).T @ np.conjugate(np.array([psi])) 
            for u in A:
                m += np.abs(np.trace(np.dot(u.todense(),rho)))
            
            mana = np.log((3**(-N)) * m)/N
            Mana.append(mana)
        
            pd.DataFrame(np.asarray(Mana)).to_csv(f'OTOC_Mana_6/Mana({depth})_sc_N{N}_d3_rand_Rot_Z_{NN}_rep_{rep}.csv')   
        rep += 1
        
    #Saving the data
    
    pd.DataFrame(np.asarray(otoc)).to_csv(f'OTOC_Mana_6/OTOC({depth})_sc_N{N}_d3_rand_Rot_Z_{NN}.csv')
#running the function for different rotation angles, theta and over all probability P in [0,1] 
<<<<<<< HEAD
NN=[5]
=======
NN=[5,10,50]
>>>>>>> 1e98111a7cb936b77f4ecec908b7031c65b5d90d

# NN = 50 has depth of [i for i in range(1,15)] so far
# NN = 20 has depth of [i for i in range(1,10)] and [i for i in range(10,100,10)] and [i for i in range(10,35,3)] so far
# NN = 10 has depth of [i for i in range(1,10)] and [i for i in range(10,100,10)] and [12,15,17], [0] (depth is 11)  so far
# NN = 5 has depth of [i for i in range(1,130,10)] + [i for i in range(130,200,10)]+[i for i in range(0,10,2)]

# NN = 50 has depth of [i for i in range(1,20)] + [15,16,17] + [18 , 19]so far
<<<<<<< HEAD
#Depth = [[1,2,11, 21, 31,  51, 61, 81, 91, 111, 130, 140, 160, 170, 190], [1, 3, 5, 9,  20, 30, 40, 50, 60, 70, 80, 90, 12,15,  17], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]]
#depth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
#for NN=5 we have depth = [5,8,15,18,210,240] for 3 reps
=======
Depth = [[1,2,11, 21, 31,  51, 61, 81, 91, 111, 130, 140, 160, 170, 190], [1, 3, 5, 9,  20, 30, 40, 50, 60, 70, 80, 90, 12,15,  17], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]]
>>>>>>> 1e98111a7cb936b77f4ecec908b7031c65b5d90d
for NN in NN:
    theta=(4*np.pi/9)*NN/50
    l = 0
    sample = 500
    
    if __name__ == '__main__':
        pool = multiprocessing.Pool()
        #rnd is the Probability of accepting non-Clifford gate
<<<<<<< HEAD
        depth = [5,8,15,18,210,240] + [1,2,11, 21, 31,  51, 61, 81, 91, 111, 130, 140, 160, 170, 190] 
=======
        depth = Depth[l]
>>>>>>> 1e98111a7cb936b77f4ecec908b7031c65b5d90d
        #depth = [5]
        outputs_async = pool.map_async(function, depth)
        outputs = outputs_async.get()
        print("Output: {}".format(outputs))
    l += 1

