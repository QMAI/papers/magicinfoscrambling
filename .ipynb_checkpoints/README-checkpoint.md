# MagicInfoScrambling

Code for the manuscript "Quantifying quantum computational complexity via information scrambling".

############################### Qutrit Code and Data ##################################################

The python script 'qutrit_Mana_OTOC.py' contains the codes required to simulate a quantum circuit for 3 dimensional Hilbert space, with the Clifford gates and non-Clifford gates of Rotation-Z and T-gate, for calculating Mana and OTOC. Depending on the availble memory of your device, you can use this code to simulate a system with handful qutrits. The generated data from this file are avalible on the folders "qutrit_Mana_vs_OTOC" and "qutrit_data" and "single_gate_data"

These data have been used on Fig.1, Fig.2, Fig.3, Fig.5 and Fig.6 of the manuscript.


############################### Qudit, d=5 code and Data ##############################################

The python script 'qudit_Mana_OTOC.py' contains the codes required to simulate a quantum circuit for 5 dimesional Hilbert space, with the Clifford gates and non-Clifford gates of Rotation-Z and T-gate, for calculating Mana and OTOC. Depending on the availble memory of your device, you can use this code to simulate a system with handful qudits. The generated data from this file are avalible on the folders "qudit_data" and "single_gate_data". 

These data have been used on Fig.3 and Fig.5 of the manuscript.

############################### Qubit Code and Data ###################################################

The python script 'chaos_qubit.py' contains the codes required to simulated a quantum circuit for 2 dimnsional Hilbert space, with the Clifford gates and time evolution of a Hamiltonian, for calculating OTOC.  Depending on the availble memory of your device, you can use this code to simulate a system with handful qubits. The genrated data from this code are avalible on the folder "chaos_data". 

These data have been used on Fig.4 of the manuscript.


The python script 'qubit_OTOC.py' contains the codes required to simulated a quantum circuit for 2 dimnsional Hilbert space, with the Clifford gates and non-Clifford gates of Rotation-Z and T-gate, for calculating OTOC.  Depending on the availble memory of your device, you can use this code to simulate a system with handful qubits. The genrated data from this code are avalible on the folder "qubit_data". 

These data have been used on Fig.2 of the manuscript.

############################## Plot codes #############################################################

the jupyter notebook 'plot.ipynb' containes the code required to plot the figures of the manuscript and its figures are avalible on the folder "plots".
