import numpy as np
from numpy.linalg import matrix_power
import scipy.sparse
import itertools

from tqdm.auto import tqdm
from numpy import linalg as LA
import pandas as pd
import multiprocessing
import cirq
from cirq.circuits import InsertStrategy
from scipy.sparse import csr_matrix
from scipy import sparse
#Number of Qubits

N = 12
qubits = [cirq.LineQubit(i) for i in range(N)]
print(qubits)
circuit = cirq.Circuit()
X = cirq.X
Z = cirq.Z
Y = cirq.Y
I = cirq.I
theta = np.pi/4
R_z = cirq.rz(theta)


sigma = [I,X,Y,Z]
n = [0, 1, 2, 3]
ind = np.array([p for p in itertools.product(n, repeat=N)])
S_collect = []
for i in tqdm(range (0, len(ind))):

    S = cirq.PauliString(sigma[ind[i][0]](qubits[0]))

    for j in range(1,N):
        S = S *cirq.PauliString(sigma[ind[i][j]](qubits[j]))  

    S_collect.append(S)


# #function of calculating OTOC for certain depth
def function (depth):
    XX = np.array([[0.,1.],
                    [1.,0.]], dtype=complex)

    ZZ = np.array([[1.,0.],
                    [0.,-1.]], dtype=complex)

    YY = np.array([[0,-1j],
                    [1j,0]], dtype=complex)
    
    
    Hadamard = cirq.H
    Phase = cirq.S
    T_gate = cirq.T
    X = cirq.X
    Z = cirq.Z
    Y = cirq.Y
    I_gate = cirq.I
    CNOT = cirq.CNOT
    single_gates = [Hadamard,Phase,X,Z,Y,I_gate]
    
    NUMBER = []
    otoc = []
    nt =[]
       
    
    rep =0
    while rep<50:
        
        circuit = cirq.Circuit()  
    
    
        #producing the random Clifford circuit with certain depth   
        for d in range(depth):
            
            depthC = 30
            for dd in range(depthC):
                print(dd)
                q = np.random.randint(0,len(single_gates),N)
                for i in range(N):
                    circuit.append(single_gates[q[i]].on(qubits[i]), strategy=InsertStrategy.NEW)


                control = []            
                while len(control)<2:
                    qq = np.random.randint(0,N,2)
                    if qq[0] != qq[1]:
                        control.append(qq)
                        circuit.append(CNOT.on(qubits[qq[0]],qubits[qq[1]]), strategy=InsertStrategy.NEW)
            
           
                        
            if np.random.rand()<1:
                t_rand = np.random.randint(0,N,1)
                circuit.append(R_z.on(qubits[t_rand[0]]), strategy=InsertStrategy.NEW)
            
            
    

        

        #unitary matrix of the circuit, U
        matrix = csr_matrix(cirq.unitary(circuit))
        
        s=cirq.Simulator()
        results=s.simulate(circuit)
        
        #final state of the circuit
        psi = csr_matrix(np.array([results.final_state_vector]).T)
        
        butterfly = csr_matrix(np.ones((1,1)))
        for i in range (N-2):
            butterfly = sparse.kron(csr_matrix(np.identity(2)),butterfly)
        butterfly = sparse.kron(butterfly,XX)
        butterfly = sparse.kron(butterfly,csr_matrix(np.identity(2)))
        
        #calculting O=U^\dagger. B.U
        O = matrix.conjugate().T @ butterfly @ matrix
        #producing M operator
        M = csr_matrix(ZZ) 
        
        for i in range(N-1):
            M = sparse.kron(M, csr_matrix(np.identity(2)))
        
        #Calculating OTOC=<O^\dagger .M^\dagger . O.M>
        OTOC = psi.conjugate().T @ O.conjugate().T @ M.conjugate().T @ O @ M @ psi
        
        #saving real part of OTOC
        otoc.append(OTOC.toarray()[0][0].real)
        print(np.array(otoc))
        M2 = []
        if rep<10:
            ss=0
            for i in range(len(S_collect)):
                st = cirq.PauliSum.from_pauli_strings([S_collect[i]])
                expectation = st.expectation_from_state_vector(psi.toarray().T [0]/np.linalg.norm(psi.toarray().T [0]),qubit_map={qubits[0]:0,qubits[1]:1,qubits[2]:2,qubits[3]:3,qubits[4]:4,qubits[5]:5,qubits[6]:6,qubits[7]:7,qubits[8]:8,
                qubits[9]:9,qubits[10]:10,qubits[11]:11 })
                
                ss += 2**(-N) * np.array(expectation).real **4
                
            m2 = -np.log(ss)/N
             
            M2.append(np.array(m2))

            pd.DataFrame(np.asarray(M2)).to_csv(f'qubit_data_final/Magic({depth})_sc_N{N}_d3_rand_Rot_Z_rep_{rep}.csv')
        print("rep = ", rep)
        rep += 1
        
        
        
       
    

        
    #Saving the data   
    pd.DataFrame(np.asarray(otoc)).to_csv(f'qubit_data_final/OTOC({depth})_sc_N{N}_d3_rand_Rot_Z.csv')
   




       
if __name__ == '__main__':

    pool = multiprocessing.Pool()
    #depth is the depth of cricuit with Clifford + T blocks
    depth = [i for i in range(2,13,2)]
    
    outputs_async = pool.map_async(function, depth)
    outputs = outputs_async.get()
    print("Output: {}".format(outputs))
